import pandas as pd
import chardet


# Detekuje kódování souboru a načte CSV soubor s tímto kódováním a oddělovačem.
def load_csv_with_detected_encoding(file_path, delimiter):
    with open(file_path, 'rb') as f:
        result = chardet.detect(f.read())
        encoding = result['encoding']
        print(f"Detected encoding for {file_path}: {encoding}")
    try:
        return pd.read_csv(file_path, encoding=encoding, delimiter=delimiter)
    except pd.errors.ParserError as e:
        print(f"Error parsing {file_path}: {e}")
        diagnose_csv_error(file_path, encoding)
        return None

# Diagnostikuje chybu při načítání CSV souboru.
def diagnose_csv_error(file_path, encoding):
    with open(file_path, 'r', encoding=encoding) as f:
        for i, line in enumerate(f, start=1):
            try:
                pd.read_csv(pd.compat.StringIO(line))
            except pd.errors.ParserError as e:
                print(f"Error on line {i}: {e}")
                break

def write_squirrel_stats(squirrel_data, stats_name):
    print()
    print(f"Vypisuji statistiky pro: {stats_name}")
    print()
    
    row_count = squirrel_data.shape[0]
    print(f"Počet záznamů (počet veverek): {row_count}")
    print()
    
    skip_columns = ["Squirrel ID", "Longitude", "Latitude", "Area ID", "Park ID", "Date", "Above Ground", "Squirrels per area"]
    
    for column in squirrel_data.columns:
        if column not in skip_columns:
            print(f"Sledovaná charakteristika: {column} (hodnota - procentuální zastoupení)")
            # Výpočet hodnot a procentuálního zastoupení
            counts = squirrel_data[column].value_counts(normalize=True)
            
            for value, percent in counts.items():
                print(f"{value}: {percent:.2%}")
            print()
        elif column == "Squirrels per area":
            print(f"Sledovaná charakteristika: {column} (průměrný počet veverek v parcích)")
            print(f"Průměr: {round(squirrel_data[column].mean(), 2)} veverek/sledovaná oblast")
            print()
        elif column == "Above Ground":
            print(f"Sledovaná charakteristika: {column} (průměrná výška veverek nad zemí)")
            selected_rows = squirrel_data.loc[squirrel_data[column] != 'Zero/Unknown']
            selected_rows.loc[:, column] = pd.to_numeric(selected_rows[column], errors='coerce')
            average = round(selected_rows[column].mean(),2)
                
            print(f"Průměr: {average:.2f} metrů nad zemí")
            print()
    
    print()
    print()
    print()
    
def write_squirrel_stats_with_park_conditions(squirrel_data, stats_name):
    print()
    print(f"Vypisuji statistiky pro: {stats_name}")
    print()
    
    row_count = squirrel_data.shape[0]
    print(f"Počet záznamů (počet veverek): {row_count}")
    print()
    
    skip_columns = ["Squirrel ID", "Longitude", "Latitude", "Area ID", "Park ID", "Date", "Above Ground", "Squirrels per area"]
    
    for column in squirrel_data.columns:
        if column not in skip_columns:
            print(f"Sledovaná charakteristika: {column} (hodnota - procentuální zastoupení)")
            # Výpočet hodnot a procentuálního zastoupení
            counts = squirrel_data[column].value_counts(normalize=True)
            
            for value, percent in counts.items():
                print(f"{value}: {percent:.2%}")
            print()
        elif column == "Squirrels per area":
            print(f"Sledovaná charakteristika: {column} (průměrný počet veverek v parcích)")
            print(f"Průměr: {round(squirrel_data[column].mean(), 2)} veverek/sledovaná oblast")
            print()
        elif column == "Above Ground":
            print(f"Sledovaná charakteristika: {column} (průměrná výška veverek nad zemí)")
            selected_rows = squirrel_data.loc[squirrel_data[column] != 'Zero/Unknown']
            selected_rows.loc[:, column] = pd.to_numeric(selected_rows[column], errors='coerce')
            average = round(selected_rows[column].mean(),2)
                
            print(f"Průměr: {average:.2f} metrů nad zemí")
            print()
        
        if column == "Temperature":
            selected_rows = squirrel_data.loc[squirrel_data[column] != 'Unknown']
            selected_rows.loc[:, column] = pd.to_numeric(selected_rows[column], errors='coerce')
            average = round(selected_rows[column].mean(),2)
                
            print(f"Průměr: {average:.2f} °C")
            print()
    
    
    print()
    print()
    print()
    
    
def write_park_stats(park_data, stats_name):
    print()
    print(f"Vypisuji statistiky pro: {stats_name}")
    print()
    
    row_count = park_data.shape[0]
    print(f"Počet záznamů (počet parků): {row_count}")
    print()
    
    parks = ', '.join(park_data['Park Name'].unique())
    print(f"Parky, kde byly veverky pozorovány: {parks}")
    print()
    
    print(f"Rozložení pozorovaných veverek mezi parky: (park: počet veverek)")
    total_squirrels = park_data['Number of Squirrels'].sum()
    for _, row in park_data.iterrows():
        park_name = row['Park Name']
        squirrels_count = row['Number of Squirrels']
        percent_of_total = (squirrels_count / total_squirrels) * 100
        print(f"{park_name}: {squirrels_count} ({percent_of_total:.2f}%)")
    print()
    
    skip_columns = ["Area ID", "Park ID", "Park Name", "Start Time", "End Time", "Number of Squirrels", "Temperature", "Date"]
    
    for column in park_data.columns:
        if column not in skip_columns:
            print(f"Sledovaná charakteristika: {column} (hodnota - procentuální zastoupení)")
            # Výpočet hodnot a procentuálního zastoupení
            counts = park_data[column].value_counts(normalize=True)
            
            for value, percent in counts.items():
                print(f"{value}: {percent:.2%}")
            print()

        elif column == "Temperature":
            print(f"Sledovaná charakteristika: {column} (teplota při pozorování)")
            selected_rows = park_data.loc[park_data[column] != 'Unknown']
            selected_rows.loc[:, column] = pd.to_numeric(selected_rows[column], errors='coerce')
            average = round(selected_rows[column].mean(),2)
                
            print(f"Průměr: {average:.2f} °C")
            print()
    
    print()
    print()
    print()

def main():
    # Načtení dat, vypsání charakteristik
    squirrel_data = load_csv_with_detected_encoding('prepared_data\squirrel_data_prepared.csv', ';')
    write_squirrel_stats(squirrel_data, "Veverky v NY parcích (bez Central Parku)")
    
    cp_squirrel_data = load_csv_with_detected_encoding('prepared_data\cp_squirrel_data_prepared.csv', ';')
    write_squirrel_stats(cp_squirrel_data, "Veverky v NY parcích (jen Central Park)")
    
    all_squirrel_data = load_csv_with_detected_encoding('prepared_data\merged_park_squirell_data_prepared.csv', ';')
    write_squirrel_stats(all_squirrel_data, "Veverky v NY parcích")
    
    park_data = load_csv_with_detected_encoding('prepared_data\park_data_prepared.csv', ';')
    write_park_stats(park_data, "Parky a prostředí")
    
    squirrel_data_with_park_conditions = load_csv_with_detected_encoding('prepared_data\squirrel_data_with_park_condition_prepared.csv', ';')
    write_squirrel_stats_with_park_conditions(squirrel_data_with_park_conditions, "Veverky v NY parcích (bez Central Parku, s podmínkami v parku)")
    
if __name__ == "__main__":
    main()