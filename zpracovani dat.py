import pandas as pd
import chardet
import csv

# Detekuje kódování souboru a načte CSV soubor s tímto kódováním a oddělovačem.
def load_csv_with_detected_encoding(file_path, delimiter):
    with open(file_path, 'rb') as f:
        result = chardet.detect(f.read())
        encoding = result['encoding']
        print(f"Detected encoding for {file_path}: {encoding}")
    try:
        return pd.read_csv(file_path, encoding=encoding, delimiter=delimiter)
    except pd.errors.ParserError as e:
        print(f"Error parsing {file_path}: {e}")
        diagnose_csv_error(file_path, encoding)
        return None

# Diagnostikuje chybu při načítání CSV souboru.
def diagnose_csv_error(file_path, encoding):
    with open(file_path, 'r', encoding=encoding) as f:
        for i, line in enumerate(f, start=1):
            try:
                pd.read_csv(pd.compat.StringIO(line))
            except pd.errors.ParserError as e:
                print(f"Error on line {i}: {e}")
                break

def print_files_info(squirrel_data, park_data, central_park_data):
    # Zobrazení prvních několika řádků každého souboru pro kontrolu
    print(squirrel_data.head())
    print(park_data.head())
    print(central_park_data.head())

    # Kontrola typů dat
    print(squirrel_data.info())
    print(park_data.info())  
    print(central_park_data.info())
        
def process_park_data(park_data):
    print("Processing park data transformation.")
    output_park_data = []

    
    for _, row in park_data.iterrows():
        new_row = {}
        # Basic data, only copy
        new_row['Area Name'] = row['Area Name']
        new_row['Area ID'] = row['Area ID']
        new_row['Park Name'] = row['Park Name']
        new_row['Park ID'] = row['Park ID']
        
        string_date = str(row['Date'])
        month = "0" + string_date[:1]
        day = "0" + string_date[2:3]
        year = string_date[4:]
        new_row['Date'] = f"{day}/{month}/{year}"
        
        new_row['Start Time'] = row['Start Time']
        new_row['End Time'] = row['End Time']
        new_row['Number of Squirrels'] = row['Number of Squirrels']
        
        # Weather data
        weather_string = str(row['Temperature & Weather']).strip()
        if weather_string[:2].isdigit():
            fahrenheit = int(weather_string[:2])
            new_row['Temperature'] = round((fahrenheit - 32) / 1.8, 2)   
        else:
            new_row['Temperature'] = "Unknown"
    
        new_row['Sunny'] = ("sunny" in weather_string)
        new_row['Windy'] = (any(val in weather_string for val in ("wind","gusts")))
        new_row['Cold'] = (any(val in weather_string for val in ("cold","chilly")))
        new_row['Dry'] = ("dry" in weather_string)
        new_row['Clear Sky'] = ("clear" in weather_string)
        new_row['Cloudy'] = ("shade spots" in weather_string)
        
        # litter
        litter_string = str(row['Litter']).strip()
        if "Some" in litter_string:
            new_row['Litter'] = "Some"
        elif "Abundant" in litter_string:
            new_row['Litter'] = "Abundant"
        else:
            new_row['Litter'] = "Unknown"
            
        # Park conditions
        park_c_string = str(row['Park Conditions']).strip()
        if "Busy" in park_c_string:
            new_row['Park Conditions'] = "Busy"
        elif "Medium" in park_c_string:
            new_row['Park Conditions'] = "Medium"
        elif any(val in park_c_string for val in ("Calm", "Cool")):
            new_row['Park Conditions'] = "Calm"
        else:
            new_row['Park Conditions'] = "Unknown"
        
        # The presence of living beings
        other_a_s_string = str(row['Other Animal Sightings']).strip()
        new_row['Humans'] = ("Human" in other_a_s_string)
        new_row['Dogs'] = ("Dog" in other_a_s_string)
        new_row['Cats'] = ("Cat" in other_a_s_string)
        new_row['Pigeons'] = ("Pigeon" in other_a_s_string)
        new_row['Hawk'] = ("Hawk" in other_a_s_string)
        new_row['Other birds'] = any(val in other_a_s_string for val in ("Dove", "Robin", "Blackbird","Sparrow","Song Birds", "Cardinal"))
        
        output_park_data.append(new_row)
        
    df = pd.DataFrame(output_park_data)
    
    print("Park data transformation finished.")
    df.to_csv('prepared_data\park_data_prepared.csv', sep=';', quotechar='"', quoting=csv.QUOTE_ALL, index=False) 
    print("New park data saved to csv file 'park_data_prepared.csv'.")
  
def process_squirrels_data_with_park_condition(squirrel_data):
    print("Processing squirrel data with park condition transformation.")
    output_squirrel_data = []
    
    for _, row in squirrel_data.iterrows():
        new_row = {}
        
        # Basic data, only copy or static value
        new_row['Area Name'] = row['Area Name']
        new_row['Area ID'] = row['Area ID']
        new_row['Park Name'] = row['Park Name']
        new_row['Park ID'] = row['Park ID']
        
        string_date = str(row['Date'])
        month = "0" + string_date[:1]
        day = "0" + string_date[2:3]
        year = string_date[4:]
        new_row['Date'] = f"{day}/{month}/{year}"

        
        new_row['Daytime'] = "PM"
        new_row['Squirrel ID'] = row['Squirrel ID']
        
        # Squirrel color
        primary_color_string = str(row['Primary Fur Color']).strip()
        if len(primary_color_string) > 3:
            new_row['Primary Color'] = primary_color_string
        else:
            new_row['Primary Color'] = "Unknown"
      
        highlights_string = str(row['Highlights in Fur Color']).strip()
        if len(highlights_string) > 3:
            new_row['Highlights in Fur'] = row['Highlights in Fur Color']
        else:
            new_row['Highlights in Fur'] = "None/Unknown"
            
        # Location
        new_row['Latitude'] = row['Squirrel Latitude (DD.DDDDDD)']
        new_row['Longitude'] = row['Squirrel Longitude (-DD.DDDDDD)']
        new_row['Squirrels per area'] = row['Number of Squirrels']
        
        location_string = str(row['Location']).strip()
        if location_string == "Above Ground" or location_string == "Ground Plane":
            new_row['Location'] = location_string
        elif location_string == "Specific Location" or location_string == "Above Ground, Specific Location" or location_string == "Ground Plane, Above Ground":
            new_row['Location'] = "Above Ground"
        elif location_string == "Ground Plane, Specific Location":
            new_row['Location'] = "Ground Plane"
        else:
            new_row['Location'] = "Unknown"
         
        above_ground_string = str(row['Above Ground (Height in Feet)']).strip()
        if above_ground_string.isdigit():
            new_row['Above Ground'] = str(round(int(above_ground_string) *  0.3048,2))
        else:
            new_row['Above Ground'] = "Zero/Unknown"
        
        # Weather data
        weather_string = str(row['Temperature & Weather']).strip()
        if weather_string[:2].isdigit():
            fahrenheit = int(weather_string[:2])
            new_row['Temperature'] = round((fahrenheit - 32) / 1.8, 2)   
        else:
            new_row['Temperature'] = "Unknown"
    
        new_row['Sunny'] = ("sunny" in weather_string)
        new_row['Windy'] = (any(val in weather_string for val in ("wind","gusts")))
        new_row['Cold'] = (any(val in weather_string for val in ("cold","chilly")))
        new_row['Dry'] = ("dry" in weather_string)
        new_row['Clear Sky'] = ("clear" in weather_string)
        new_row['Cloudy'] = ("shade spots" in weather_string)
        
        # litter
        litter_string = str(row['Litter']).strip()
        if "Some" in litter_string:
            new_row['Litter'] = "Some"
        elif "Abundant" in litter_string:
            new_row['Litter'] = "Abundant"
        else:
            new_row['Litter'] = "Unknown"
        
        # Activites
        activites_string = str(row['Activities']).strip()
        new_row['Running'] = ("Running" in activites_string)
        new_row['Chasing'] = ("Chasing" in activites_string)
        new_row['Climbing'] = ("Climbing" in activites_string)
        new_row['Eating'] = ("Eating" in activites_string)
        new_row['Foraging'] = ("Foraging" in activites_string)
        
        # Interactions with Humans
        interactions_string = str(row['Interactions with Humans']).strip()
        new_row['Approaches'] = ("Approaches" in interactions_string)
        new_row['Indifferent'] = ("Indifferent" in interactions_string)
        new_row['Runs From'] = ("Runs From" in interactions_string)
        
        # Park conditions
        park_c_string = str(row['Park Conditions']).strip()
        if "Busy" in park_c_string:
            new_row['Park Conditions'] = "Busy"
        elif "Medium" in park_c_string:
            new_row['Park Conditions'] = "Medium"
        elif any(val in park_c_string for val in ("Calm", "Cool")):
            new_row['Park Conditions'] = "Calm"
        else:
            new_row['Park Conditions'] = "Unknown"
        
        # The presence of living beings
        other_a_s_string = str(row['Other Animal Sightings']).strip()
        new_row['Humans'] = ("Human" in other_a_s_string)
        new_row['Dogs'] = ("Dog" in other_a_s_string)
        new_row['Cats'] = ("Cat" in other_a_s_string)
        new_row['Pigeons'] = ("Pigeon" in other_a_s_string)
        new_row['Hawk'] = ("Hawk" in other_a_s_string)
        new_row['Other birds'] = any(val in other_a_s_string for val in ("Dove", "Robin", "Blackbird","Sparrow","Song Birds", "Cardinal"))
        
        
        output_squirrel_data.append(new_row)
        
    df = pd.DataFrame(output_squirrel_data)
    
    print("Squirrel data with park condition transformation finished.")
    df.to_csv('prepared_data\squirrel_data_with_park_condition_prepared.csv', sep=';', quotechar='"', quoting=csv.QUOTE_ALL, index=False) 
    print("New squirrel data with park condition data saved to csv file 'squirrel_data_with_park_condition_prepared.csv'.")  
  
  
  
def process_squirrels_data(squirrel_data):
    print("Processing squirrel data transformation.")
    output_squirrel_data = []
    
    for _, row in squirrel_data.iterrows():
        new_row = {}
        
        # Basic data, only copy or static value
        new_row['Area Name'] = row['Area Name']
        new_row['Area ID'] = row['Area ID']
        new_row['Park Name'] = row['Park Name']
        new_row['Park ID'] = row['Park ID']
        
        string_date = str(row['Date'])
        month = "0" + string_date[:1]
        day = "0" + string_date[2:3]
        year = string_date[4:]
        new_row['Date'] = f"{day}/{month}/{year}"

        
        new_row['Daytime'] = "PM"
        new_row['Squirrel ID'] = row['Squirrel ID']
        
        # Squirrel color
        primary_color_string = str(row['Primary Fur Color']).strip()
        
        if len(primary_color_string) > 3:
            new_row['Primary Color'] = primary_color_string
        else:
            new_row['Primary Color'] = "Unknown"
      
        highlights_string = str(row['Highlights in Fur Color']).strip()
        if len(highlights_string) > 3:
            new_row['Highlights in Fur'] = row['Highlights in Fur Color']
        else:
            new_row['Highlights in Fur'] = "None/Unknown"
            
        # Location
        new_row['Latitude'] = row['Squirrel Latitude (DD.DDDDDD)']
        new_row['Longitude'] = row['Squirrel Longitude (-DD.DDDDDD)']
        new_row['Squirrels per area'] = row['Number of Squirrels']
        
        location_string = str(row['Location']).strip()
        if location_string == "Above Ground" or location_string == "Ground Plane":
            new_row['Location'] = location_string
        elif location_string == "Specific Location" or location_string == "Above Ground, Specific Location" or location_string == "Ground Plane, Above Ground":
            new_row['Location'] = "Above Ground"
        elif location_string == "Ground Plane, Specific Location":
            new_row['Location'] = "Ground Plane"
        else:
            new_row['Location'] = "Unknown"
         
        above_ground_string = str(row['Above Ground (Height in Feet)']).strip()
        if above_ground_string.isdigit():
            new_row['Above Ground'] = str(round(int(above_ground_string) *  0.3048,2))
        else:
            new_row['Above Ground'] = "Zero/Unknown"
        
        # Activites
        activites_string = str(row['Activities']).strip()
        new_row['Running'] = ("Running" in activites_string)
        new_row['Chasing'] = ("Chasing" in activites_string)
        new_row['Climbing'] = ("Climbing" in activites_string)
        new_row['Eating'] = ("Eating" in activites_string)
        new_row['Foraging'] = ("Foraging" in activites_string)
        
        # Interactions with Humans
        interactions_string = str(row['Interactions with Humans']).strip()
        new_row['Approaches'] = ("Approaches" in interactions_string)
        new_row['Indifferent'] = ("Indifferent" in interactions_string)
        new_row['Runs From'] = ("Runs From" in interactions_string)
        
        output_squirrel_data.append(new_row)
        
    df = pd.DataFrame(output_squirrel_data)
    
    print("Squirrel data transformation finished.")
    df.to_csv('prepared_data\squirrel_data_prepared.csv', sep=';', quotechar='"', quoting=csv.QUOTE_ALL, index=False) 
    print("New squirrel data saved to csv file 'squirrel_data_prepared.csv'.")

    return df
        

def process_central_park_data(central_park_data):
    print("Processing central park squirrel data transformation.")
    output_central_park_data = []
    
    for _, row in central_park_data.iterrows():
        new_row = {}
        
        # Basic data, only copy or static value
        new_row['Area Name'] = "CENTRAL MANHATTAN"
        new_row['Area ID'] = "E"
        new_row['Park Name'] = "Central Park"
        new_row['Park ID'] = "25"
        
        # date
        string_date = str(row['Date'])
        month = string_date[:2]
        day = string_date[2:4]
        year = string_date[6:]
        new_row['Date'] = f"{day}/{month}/{year}"
        
        new_row['Daytime'] = row['Shift']
        
        new_row['Squirrel ID'] = row['Unique Squirrel ID']
        
        # Squirrel color
        primary_color_string = str(row['Primary Fur Color']).strip()
        if len(primary_color_string) > 3:
            new_row['Primary Color'] = primary_color_string
        else:
            new_row['Primary Color'] = "Unknown"
        
        highlights_string = str(row['Highlight Fur Color']).strip()
        if len(highlights_string) > 3:
            new_row['Highlights in Fur'] = row['Highlight Fur Color']
        else:
            new_row['Highlights in Fur'] = "None/Unknown"
            
        # Location
        new_row['Latitude'] = row['Y']
        new_row['Longitude'] = row['X']
        new_row['Squirrels per area'] = row['Hectare Squirrel Number']
        
        location_string = str(row['Location']).strip()
        if location_string == "Above Ground" or location_string == "Ground Plane":
            new_row['Location'] = location_string
        else:
            new_row['Location'] = "Unknown"
         
        above_ground_string = str(row['Above Ground Sighter Measurement']).strip()
        if above_ground_string.isdigit():
            new_row['Above Ground'] = str(round(int(above_ground_string) *  0.3048,2))
        else:
            new_row['Above Ground'] = "Zero/Unknown"
        
        # Activites
        new_row['Running'] = row['Running']
        new_row['Chasing'] = row['Chasing']
        new_row['Climbing'] = row['Climbing']
        new_row['Eating'] = row['Eating']
        new_row['Foraging'] = row['Foraging']
        
        # Interactions with Humans
        new_row['Approaches'] = row['Approaches']
        new_row['Indifferent'] = row['Indifferent']
        new_row['Runs From'] = row['Runs from']
        
        output_central_park_data.append(new_row)
        
    df = pd.DataFrame(output_central_park_data)
    
    print("Central park squirrel data transformation finished.")
    df.to_csv('prepared_data\cp_squirrel_data_prepared.csv', sep=';', quotechar='"', quoting=csv.QUOTE_ALL, index=False) 
    print("New central park squirrel data saved to csv file 'cp_squirrel_data_prepared.csv'.")

    return df


def main():
    # Načtení dat ze všech CSV souborů
    squirrel_data = load_csv_with_detected_encoding('sources\squirrel_data.csv', ',')
    park_data = load_csv_with_detected_encoding('sources\park_data.csv', ';')
    central_park_data = load_csv_with_detected_encoding('sources\central_park_data.csv', ';')
    
    # Zobrazení informací o načtených datech
    #print_files_info(squirrel_data, park_data, central_park_data)
    
    process_park_data(park_data)
    
    # Spojení dat squirrel_data s park_data na základě společných údajů v souborech
    squirrel_data = squirrel_data.merge(park_data, on=['Area Name', 'Area ID', 'Park Name', 'Park ID'], how='left')
    #squirrel_data.to_csv('prepared_data\data_prepared.csv', sep=';', quotechar='"', quoting=csv.QUOTE_ALL, index=False)

    # Data o veverkách spolu s počasím a dalšími podmínkami z parku
    process_squirrels_data_with_park_condition(squirrel_data)

    # Transformace dat o veverkách z central parku a ostatních parků
    new_squirrel_data = process_squirrels_data(squirrel_data)
    new_squirrel_cp_data = process_central_park_data(central_park_data)

    # Spojení dat z central parku a ostatních parků do jednoho csv souboru
    print("Merging squirrel data from both origins.")
    all_squirrel_data = pd.concat([new_squirrel_data, new_squirrel_cp_data], ignore_index=True)
    print("Data merged, creating csv...")
    all_squirrel_data.to_csv('prepared_data\merged_park_squirell_data_prepared.csv', sep=';', quotechar='"', quoting=csv.QUOTE_ALL, index=False)
    print("Merged csv 'merged_park_squirell_data_prepared.csv' created.")
    
if __name__ == "__main__":
    main()