import pandas as pd
import chardet
from scipy.stats import pearsonr
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import ttest_ind
import statsmodels.api as sm
from statsmodels.formula.api import ols



# Detekuje kódování souboru a načte CSV soubor s tímto kódováním a oddělovačem.
def load_csv_with_detected_encoding(file_path, delimiter):
    with open(file_path, 'rb') as f:
        result = chardet.detect(f.read())
        encoding = result['encoding']
        #print(f"Detected encoding for {file_path}: {encoding}")
    try:
        return pd.read_csv(file_path, encoding=encoding, delimiter=delimiter)
    except pd.errors.ParserError as e:
        print(f"Error parsing {file_path}: {e}")
        diagnose_csv_error(file_path, encoding)
        return None

# Diagnostikuje chybu při načítání CSV souboru.
def diagnose_csv_error(file_path, encoding):
    with open(file_path, 'r', encoding=encoding) as f:
        for i, line in enumerate(f, start=1):
            try:
                pd.read_csv(pd.compat.StringIO(line))
            except pd.errors.ParserError as e:
                print(f"Error on line {i}: {e}")
                break
            

def most_eating_squirrels(squirrel_data):
    # Filtrování řádků, kde Eating je true
    eating_squirrels = squirrel_data[squirrel_data['Eating'] == True]

    # Spočítání počtu řádků (veverek, které jedí) pro každý park
    eating_counts = eating_squirrels.groupby('Park Name').size()

    # Spočítání celkového počtu veverek pro každý park
    total_squirrels_per_park = squirrel_data.groupby('Park Name').size()

    # Normalizace počtu veverek, které jedí, podle celkového počtu veverek v parku
    normalized_eating_counts = (eating_counts / total_squirrels_per_park * 100).fillna(0)

    # Nalezení deseti parků s nejvyššími normalizovanými počty veverek, které jedí
    top_parks = normalized_eating_counts.nlargest(10)

    print("Seznam top deseti parků, kde bylo pozorováno nejvíce veverek, jak jí:\n(v poměru k počtu veverek na park)")
    print()
    for park in top_parks.index:
        print(f"{park}: {top_parks[park]:.2f} % , celkem veverek v parku: {total_squirrels_per_park[park]}")
    print()
    print()
        
def most_friendly_squirrels(squirrel_data):
    total_squirrels_per_park = squirrel_data.groupby('Park Name').size()
    
    approaching_squirrels = squirrel_data[squirrel_data['Approaches'] == True]
    approaching_counts = approaching_squirrels.groupby('Park Name').size()
    normalized_approaching_counts = (approaching_counts / total_squirrels_per_park * 100).fillna(0)

    top_approaching_parks = normalized_approaching_counts.nlargest(10)

    print("Seznam top deseti parků, kde bylo pozorováno nejvíce veverek přibližujících se k člověku:\n(v poměru k počtu veverek na park)")
    print()
    for park in top_approaching_parks.index:
        print(f"{park}: {top_approaching_parks[park]:.2f} % , celkem veverek v parku: {total_squirrels_per_park[park]}")
    print()
    print()
    
    indifferent_squirrels = squirrel_data[squirrel_data['Indifferent'] == True]
    indifferent_counts = indifferent_squirrels.groupby('Park Name').size()
    normalized_indifferent_counts = (indifferent_counts / total_squirrels_per_park * 100).fillna(0)

    top_indifferent_parks = normalized_indifferent_counts.nlargest(10)

    print("Seznam top deseti parků, kde bylo pozorováno nejvíce lhostejných veverek:\n(v poměru k počtu veverek na park)")
    print()
    for park in top_indifferent_parks.index:
        print(f"{park}: {top_indifferent_parks[park]:.2f} % , celkem veverek v parku: {total_squirrels_per_park[park]}")
    print()
    print()
    
    runing_squirrels = squirrel_data[squirrel_data['Runs From'] == True]
    runing_counts = runing_squirrels.groupby('Park Name').size()
    normalized_runing_counts = (runing_counts / total_squirrels_per_park * 100).fillna(0)

    top_runing_parks = normalized_runing_counts.nlargest(10)

    print("Seznam top deseti parků, kde bylo pozorováno nejvíce veverek, které utíkají od lidí:\n(v poměru k počtu veverek na park)")
    print()
    for park in top_runing_parks.index:
        print(f"{park}: {top_runing_parks[park]:.2f} % , celkem veverek v parku: {total_squirrels_per_park[park]}")
    print()
    print()
    
       
    
    average_rank = (normalized_approaching_counts.rank() - normalized_runing_counts.rank() - (normalized_indifferent_counts.rank()/2))
    
    sorted_parks = average_rank.sort_values()
    """
    print("Seznam parků podle průměrného pořadí přátelství veverek:")
    for park in sorted_parks.index:
        print(f"{park}: Průměrné pořadí: {sorted_parks[park]:.2f}")
    """
    
    top_friendly_parks = average_rank.nlargest(10)

    print("Seznam top deseti parků, kde jsou nejpřátelštější veverky:\n(v poměru k počtu veverek na park)")
    print()
    for park in top_friendly_parks.index:
        print(f"{park}: {top_friendly_parks[park]:.2f} (score), celkem veverek v parku: {total_squirrels_per_park[park]}")
    print()
    print()

def most_squirrels_onground(squirrel_data):
    grounded_squirrels = squirrel_data[squirrel_data['Location'] == "Ground Plane"]

    grounded_counts = grounded_squirrels.groupby('Park Name').size()

    total_squirrels_per_park = squirrel_data.groupby('Park Name').size()

    normalized_grounded_counts = (grounded_counts / total_squirrels_per_park * 100).fillna(0)

    top_grounded_parks = normalized_grounded_counts.nlargest(10)

    print("Seznam top deseti parků, kde bylo pozorováno nejvíce veverek na zemi:\n(v poměru k počtu veverek na park)")
    print()
    for park in top_grounded_parks.index:
        print(f"{park}: {top_grounded_parks[park]:.2f} % , celkem veverek v parku: {total_squirrels_per_park[park]}")
    print()
    print()
    
def most_cinnamon_squirrels(squirrel_data):
    cinnamon_squirrels = squirrel_data[squirrel_data['Primary Color'] == "Cinnamon"]

    cinnamon_counts = cinnamon_squirrels.groupby('Park Name').size()

    total_squirrels_per_park = squirrel_data.groupby('Park Name').size()

    normalized_cinnamon_counts = (cinnamon_counts / total_squirrels_per_park * 100).fillna(0)

    top_cinnamon_parks = normalized_cinnamon_counts.nlargest(5)

    print("Seznam top pěti parků, kde bylo pozorováno nejvíce skořicových veverek:\n(v poměru k počtu veverek na park)")
    print()
    for park in top_cinnamon_parks.index:
        print(f"{park}: {top_cinnamon_parks[park]:.2f} % , celkem veverek v parku: {total_squirrels_per_park[park]}")
    print()
    print()
    

def chi_square(squirrel_data):
    print("Chi kvadrát test pro zjištění závislosti mezi barvou veverky a parkem:")
    print()
    # Vytvoření kontingenční tabulky
    contingency_table = pd.crosstab(squirrel_data['Park Name'], squirrel_data['Primary Color'])
    print(contingency_table)

    # Chi-kvadrát test nezávislosti
    chi2, p, dof, expected = stats.chi2_contingency(contingency_table)

    print(f'Chi2 stat: {chi2}')
    print(f'p-value: {p}')
    print(f'Degrees of freedom: {dof}')
    #print('Expected frequencies:')
    #print(expected)

    # Heatmapa kontingenční tabulky
    """
    sns.heatmap(contingency_table, annot=True, cmap="YlGnBu", cbar=False)
    plt.title('Park vs Fur Color')
    plt.show()
    """
    print()
    print()
    print()

def number_of_squrrel_and_temperature(df):
    print("Výpočet kovariance a korelace pro zjištění nezávislosti teploty a počtu viděných veverek:")
    print()
    
    df_clean = df[df['Temperature'] != 'Unknown'].copy()

    # Převedení sloupce 'temperature' na číselný typ
    df_clean.loc[:, 'Temperature'] = pd.to_numeric(df_clean['Temperature'])

    # Výpočet kovariance
    covariance = df_clean['Number of Squirrels'].cov(df_clean['Temperature'])
    print(f'Kovariance: {covariance}')

    # Výpočet korelace
    correlation, p_value = pearsonr(df_clean['Number of Squirrels'], df_clean['Temperature'])
    print(f'Pearsonova korelace: {correlation}')
    print(f'p-hodnota: {p_value}')
    
    # Výklad výsledků
    print("\nVýklad výsledků:")
    if covariance > 0:
        print(f"Kovariance je kladná ({covariance}).")
        print("To znamená, že s rostoucí teplotou má tendenci růst i počet viděných veverek.")
    elif covariance < 0:
        print(f"Kovariance je záporná ({covariance}).")
        print("To znamená, že s rostoucí teplotou má tendenci klesat počet viděných veverek.")
    else:
        print(f"Kovariance je nulová ({covariance}).")
        print("To znamená, že mezi teplotou a počtem viděných veverek není žádná závislost.")

    print()
    if p_value < 0.05:
        if correlation > 0:
            print("Korelace je statisticky významná a kladná.")
            print("To znamená, že s rostoucí teplotou roste i počet viděných veverek.")
        elif correlation < 0:
            print("Korelace je statisticky významná a záporná.")
            print("To znamená, že s rostoucí teplotou klesá počet viděných veverek.")
        else:
            print("Korelace je statisticky významná, ale hodnota korelace je nulová.")
            print("To znamená, že mezi teplotou a počtem viděných veverek není lineární vztah.")
    else:
        print("Korelace není statisticky významná (p-hodnota > 0.05).")
        print("To znamená, že nelze tvrdit, že mezi teplotou a počtem viděných veverek existuje statisticky významný lineární vztah.")
    print()
    print()
    print()
 
def t_test_sunny_squirrel_number(df):
    # Rozdělení dat do dvou skupin podle slunečných dnů
    sunny_days = df[df['Sunny'] == True]['Number of Squirrels']
    non_sunny_days = df[df['Sunny'] == False]['Number of Squirrels']

    # Provedení t-testu
    t_stat, p_value = ttest_ind(sunny_days, non_sunny_days)
    print(f'T-test: Porovnání počtu veverek mezi slunečnými a ne slunečnými dny')
    print(f't-statistika: {t_stat}')
    print(f'p-hodnota: {p_value}')
    if p_value < 0.05:
        print("P-hodnota je menší než 0.05, což naznačuje, že existuje statisticky významný rozdíl v počtu veverek mezi slunečnými a neslunečnými dny.")
    else:
        print("P-hodnota je větší než 0.05, což naznačuje, že není dostatek důkazů pro to, aby se tvrdilo, že existuje statisticky významný rozdíl v počtu veverek mezi slunečnými a neslunečnými dny.")
    print()
    print()
    print()
    
def t_test_windy_squirrel_number(df):
    # Rozdělení dat do dvou skupin podle slunečných dnů
    sunny_days = df[df['Windy'] == True]['Number of Squirrels']
    non_sunny_days = df[df['Windy'] == False]['Number of Squirrels']

    # Provedení t-testu
    t_stat, p_value = ttest_ind(sunny_days, non_sunny_days)
    print(f'T-test: Porovnání počtu veverek mezi slunečnými a ne slunečnými dny')
    print(f't-statistika: {t_stat}')
    print(f'p-hodnota: {p_value}')
    if p_value < 0.05:
        print("P-hodnota je menší než 0.05, což naznačuje, že existuje statisticky významný rozdíl v počtu veverek mezi větrnými a jinými dny.")
    else:
        print("P-hodnota je větší než 0.05, což naznačuje, že není dostatek důkazů pro to, aby se tvrdilo, že existuje statisticky významný rozdíl v počtu veverek mezi větrnými a jinými dny.")
    print()
    print()
    print()

def anova_sunny_squirrel_number(df):
    df['Sunny'] = df['Sunny'].astype(int)

    # Vytvoření modelu a provedení ANOVA
    formula = 'Q("Number of Squirrels") ~ C(Sunny)'
    model = ols(formula, data=df).fit()
    anova_table = sm.stats.anova_lm(model, typ=2)
    print("Analýza rozptylu (ANOVA):")
    print(anova_table)
    p_value = anova_table.loc['C(Sunny)', 'PR(>F)']
    if p_value < 0.05:
        print("P-hodnota je menší než 0.05, což naznačuje, že existuje statisticky významný rozdíl v průměrném počtu veverek mezi slunečnými a neslunečnými dny.")
    else:
        print("P-hodnota je větší než 0.05, což naznačuje, že není dostatek důkazů pro to, aby se tvrdilo, že existuje statisticky významný rozdíl v průměrném počtu veverek mezi slunečnými a neslunečnými dny.")
    print()
    print()
    print() 
        
def anova_windy_squirrel_number(df):
    df['Windy'] = df['Windy'].astype(int)

    # Vytvoření modelu a provedení ANOVA
    formula = 'Q("Number of Squirrels") ~ C(Windy)'
    model = ols(formula, data=df).fit()
    anova_table = sm.stats.anova_lm(model, typ=2)
    print("Analýza rozptylu (ANOVA):")
    print(anova_table)
    p_value = anova_table.loc['C(Windy)', 'PR(>F)']
    if p_value < 0.05:
        print("P-hodnota je menší než 0.05, což naznačuje, že existuje statisticky významný rozdíl v průměrném počtu veverek mezi větrnými a jinými dny.")
    else:
        print("P-hodnota je větší než 0.05, což naznačuje, že není dostatek důkazů pro to, aby se tvrdilo, že existuje statisticky významný rozdíl v průměrném počtu veverek mezi větrnými a jinými dny.")
    print()
    print()
    print()   
        
def main():
    # Načtení dat
    #squirrel_data = load_csv_with_detected_encoding('prepared_data\squirrel_data_prepared.csv', ';')
    #cp_squirrel_data = load_csv_with_detected_encoding('prepared_data\cp_squirrel_data_prepared.csv', ';')
    
    
    all_squirrel_data = load_csv_with_detected_encoding('prepared_data\merged_park_squirell_data_prepared.csv', ';')
    most_eating_squirrels(all_squirrel_data)
    most_friendly_squirrels(all_squirrel_data)
    most_squirrels_onground(all_squirrel_data)
    most_cinnamon_squirrels(all_squirrel_data)
    chi_square(all_squirrel_data)
    
    park_data = load_csv_with_detected_encoding('prepared_data\park_data_prepared.csv', ';')
    number_of_squrrel_and_temperature(park_data)
    t_test_sunny_squirrel_number(park_data)
    anova_sunny_squirrel_number(park_data)
    
    t_test_windy_squirrel_number(park_data)
    anova_windy_squirrel_number(park_data)
   
    #squirrel_data_with_park_conditions = load_csv_with_detected_encoding('prepared_data\squirrel_data_with_park_condition_prepared.csv', ';')
    
    
    
if __name__ == "__main__":
    main()